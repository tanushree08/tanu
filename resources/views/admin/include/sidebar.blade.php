    <!-- ========== Left Sidebar Start ========== -->

    <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>

                        	<li class="text-muted menu-title">Navigation</li>

                            <li class="has_sub">
                                <a href="{{ url('html/dashboard') }}" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span> <span class="menu-arrow"></span></a>
                                
                            </li>

                            <li class="has_sub">
                                <a href="{{ url('html/product') }}" class="waves-effect"><i class="ti-paint-bucket"></i> <span> List Of Product </span> <span class="menu-arrow"></span> </a>
                                
                            </li>
                            <li class="has_sub">
                                <a href="{{ url('html/add-product') }}" class="waves-effect"><i class="ti-paint-bucket"></i> <span>Add Product </span> <span class="menu-arrow"></span> </a>
                                
                            </li>

                            <li class="has_sub">
                                <a href="{{ url('html/order') }}" class="waves-effect"><i class="ti-light-bulb"></i><span class="label label-primary pull-right">9</span><span> Order List </span> </a>
                               
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-spray"></i> <span> Payment Transaction </span> <span class="menu-arrow"></span> </a>
                               
                            </li>
                            <li class="has_sub">
                                <a href="{{ url('html/profile') }}" class="waves-effect"><i class="ti-spray"></i> <span> My Profile </span> <span class="menu-arrow"></span> </a>
                               
                            </li>

                            

                           

                           

                           

                            

                            

                            
                           
                            

                            
                           

                            
                           

                            
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End -->