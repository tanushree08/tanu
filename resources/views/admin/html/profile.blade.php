@extends('admin.layout.master')
@section('head-title', 'Seller Profile')
@section('content')
<!-- Start content -->
<div class="content">

<div class="wraper container">

    <!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
       

        <h4 class="page-title">Profile</h4>
        <ol class="breadcrumb">
            <li><a href="#">Edhik</a></li>
            <li><a href="#">Extras</a></li>
            <li class="active">Profile</li>
        </ol>
    </div>
</div>

    <div class="row">
        <div class="col-md-8 col-lg-8">
            <div class="profile-detail card-box">
                <div>
                    

                    

                    

                   
                    <h4 class="text-uppercase font-600">About Me</h4>
                    <div class="btn-group pull-right m-t-15">
            <a href="{{ url('html/profile-edit') }}" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Edit Profile <span class="m-l-5"><i class="fa fa-cog"></i></span></a>
            
        </div>
                   

                    <div class="text-left">
                        <p class="text-muted font-13"><strong>Full Name :</strong> <span class="m-l-15">Tanushree Sahu</span></p>

                        <p class="text-muted font-13"><strong>Company :</strong><span class="m-l-15">(123) 123 1234</span></p>

                        <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">tanu@gmail.com</span></p>

                        <p class="text-muted font-13"><strong>Phone no :</strong> <span class="m-l-15">India</span></p>
                        <p class="text-muted font-13"><strong>GST :</strong> <span class="m-l-15">India</span></p>
                        <p class="text-muted font-13"><strong>PAN Card :</strong> <span class="m-l-15">India</span></p>
                        <p class="text-muted font-13"><strong>Address 1</strong> <span class="m-l-15">India</span></p>
                        <p class="text-muted font-13"><strong>Address 2</strong> <span class="m-l-15">India</span></p>
                        <p class="text-muted font-13"><strong>City / pincode :</strong> <span class="m-l-15">India</span></p>
                        <p class="text-muted font-13"><strong>State</strong> <span class="m-l-15">India</span></p>
                        <p class="text-muted font-13"><strong>Country</strong> <span class="m-l-15">India</span></p>

                    </div>


                    
                </div>

            </div>

            
        </div>


        

    </div>



</div> <!-- container -->
           
</div> <!-- content -->
@endsection