@extends('admin.layout.master')
@section('head-title', 'manage profile')
@section('content')
<!-- Start content -->
  <!-- Start content -->
  <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Sellers</h4>
                                <ol class="breadcrumb">
                                    <li><a href="#">Edhik</a></li>
                                    <li><a href="#">eCommerce</a></li>
                                    <li class="active">Sellers</li>
                                </ol>
                            </div>
                        </div>
                        
                        <div class="row">
                        	<div class="col-lg-12">
                        		<div class="card-box">
                        			<div class="row">
			                        	<div class="col-sm-8">
			                        		<form role="form">
			                                    <div class="form-group contact-search m-b-30">
			                                    	<input type="text" id="search" class="form-control" placeholder="Search...">
			                                        <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
			                                    </div> <!-- form-group -->
			                                </form>
			                        	</div>
			                        	<div class="col-sm-4">
			                        		 <a href="#custom-modal" class="btn btn-default btn-md waves-effect waves-light m-b-30" data-animation="fadein" data-plugin="custommodal" 
			                                                    	data-overlaySpeed="200" data-overlayColor="#36404a"><i class="md md-add"></i> Add Seller</a>
			                        	</div>
			                        </div>
			                        
                        			<div class="table-responsive">
                                        <table class="table table-hover mails m-0 table table-actions-bar">
                                        	<thead>
												<tr>
													<th style="min-width: 95px;">
														Image
                                                        
													</th>
													<th>Product Name</th>
													<th>Category</th>
                                                    <th>MRP/ Selling Price</th>
													<th>Our Price</th>
													<th style="min-width: 90px;">Action</th>
												</tr>
											</thead>
											
                                            <tbody>
                                                <tr class="active">
                                                    <td>
                                                        
                                                        
                                                        <img src="assets/images/users/avatar-2.jpg"   class="img-circle thumb-sm" />
                                                    </td>
                                                    
                                                    <td>
                                                        Tomaslau
                                                    </td>
                                                    
                                                    <td>
                                                        <a href="#">tomaslau@dummy.com</a>
                                                    </td>

                                                    <td>
                                                        <b><a href="" class="text-dark"><b>356</b></a> </b>
                                                    </td>
                                                    
                                                    <td>
                                                        01/11/2003
                                                    </td>
                                                    <td>
                                                    	<a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                                    	<a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>
                                                       
                                                        
                                                        <img src="assets/images/users/avatar-1.jpg"  class="img-circle thumb-sm" />
                                                    </td>
                                                    
                                                    <td>
                                                        Chadengle
                                                    </td>
                                                    
                                                    <td>
                                                        <a href="#">chadengle@dummy.com</a>
                                                    </td>

                                                    <td>
                                                        <b><a href="" class="text-dark"><b>568</b></a> </b>
                                                    </td>
                                                    
                                                    <td>
                                                        01/11/2003
                                                    </td>
                                                    <td>
                                                    	<a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                                    	<a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                                                    </td>
                                                </tr>
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                               
                                                
                                                
                                                
                                              
                                                
                                            
                                            </tbody>
                                        </table>
                                    </div>
                        		</div>
                                
                            </div> <!-- end col -->

                            
                        </div>

                        
                        
                        

                    </div> <!-- container -->
                               
                </div> <!-- content -->
@endsection