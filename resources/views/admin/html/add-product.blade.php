@extends('admin.layout.master')
@section('head-title', 'Seller Profile')
@section('content')
<!-- Start content -->
  <!-- Start content -->
  <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Add Product</h4>
                                <ol class="breadcrumb">
                                    
                                    <li><a href="#">Add Product</a></li>
                                  
                                </ol>
                            </div>
                        </div>
                        
                        <div class="row">
                        	<div class="col-lg-12">
                        		<div class="card-box">
                        			<div class="row">
			                        	<div class="col-sm-8">
			                        		<form role="form">
			                                    <div class="form-group contact-search m-b-30">
			                                    	<input type="text" id="search" class="form-control" placeholder="Search...">
			                                        <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
			                                    </div> <!-- form-group -->
			                                </form>
			                        	</div>
			                        	<div class="col-sm-4">
			                        		 <a href="" class="btn btn-default btn-md waves-effect waves-light m-b-30" data-animation="fadein" data-plugin="custommodal" 
			                                                    	data-overlaySpeed="200" data-overlayColor="#36404a"><i class="md md-add"></i> Search</a>
			                        	</div>
			                        </div>
                                    
			                        <div class="row">
                                    <div class="col-sm-3">
                                    <a href="#amazon" class="btn btn-block btn-lg btn-primary waves-effect waves-light"  data-animation="fadein" data-plugin="custommodal" 
			                                                    	data-overlaySpeed="200" data-overlayColor="#36404a"><i class="md md-add"></i> From Amazon</a>
                                        </div>
                                        <div class="col-sm-3">
                                    <a href="#flipkart" class="btn btn-block btn-lg btn-pink waves-effect waves-light"  data-animation="fadein" data-plugin="custommodal" 
			                                                    	data-overlaySpeed="200" data-overlayColor="#36404a"><i class="md md-add"></i> From Flipkart</a>
                                        </div>
                                        <div class="col-sm-3">
                                    <a href="#custom" class="btn btn-block btn-lg btn-success waves-effect waves-light"  data-animation="fadein" data-plugin="custommodal" 
			                                                    	data-overlaySpeed="200" data-overlayColor="#36404a"><i class="md md-add"></i> Custom Product</a>
                                        </div>
                                       
                                    </div>
                        			<div class="table-responsive">
                                        <table class="table table-hover mails m-0 table table-actions-bar">
                                        	<thead>
												<tr>
													
													<th>Product Name/link</th>
													<th>Source</th>
                                                    <th>Price</th>
													<th>Request  Date</th>
													<th style="min-width: 90px;">Action</th>
												</tr>
											</thead>
											
                                            <tbody>
                                                <tr>
                                                    
                                                    
                                                    <td>
                                                        Tomaslau
                                                    </td>
                                                    
                                                    <td>
                                                        <a href="#">tomaslau@dummy.com</a>
                                                    </td>

                                                    <td>
                                                        <b><a href="" class="text-dark"><b>356</b></a> </b>
                                                    </td>
                                                    
                                                    <td>
                                                        01/11/2003
                                                    </td>
                                                    <td>
                                                    	<a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                                    	<a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                                                    </td>
                                                </tr>
                                                
                                       
                                                
                                            
                                            </tbody>
                                        </table>
                                    </div>
                        		</div>
                                
                            </div> <!-- end col -->

                            
                        </div>

                        
                        
                        

                    </div> <!-- container -->
                               
                </div> <!-- content -->
                <div id="amazon" class="modal-demo">
			    <button type="button" class="close" onclick="Custombox.close();">
			        <span>&times;</span><span class="sr-only">Close</span>
			    </button>
			    <h4 class="custom-modal-title">Add amazon's Product</h4>
			    <div class="custom-modal-text text-left">
			        <form role="form">
			        	<div class="form-group">
                            <label for="name">Product Link</label>
                            <textarea class="form-control" rows="5"></textarea>
                        </div>
                        
                       
                        
                        <div class="form-group">
                            <label for="position">Product Price</label>
                            <input type="text" class="form-control" id="position" placeholder="Product Price">
                        </div>
                        
                        
                        <button type="submit" class="btn btn-default waves-effect waves-light">Save</button>
                        <button type="button" class="btn btn-danger waves-effect waves-light m-l-10">Cancel</button>
                    </form>
			    </div>
			</div>
            <div id="flipkart" class="modal-demo">
			    <button type="button" class="close" onclick="Custombox.close();">
			        <span>&times;</span><span class="sr-only">Close</span>
			    </button>
			    <h4 class="custom-modal-title">Add Flipkart's Product</h4>
			    <div class="custom-modal-text text-left">
			        <form role="form">
                    <div class="form-group">
                            <label for="name">Product Link</label>
                            <textarea class="form-control" rows="5"></textarea>
                        </div>
                        
                       
                        
                        <div class="form-group">
                            <label for="position">Product Price</label>
                            <input type="text" class="form-control" id="position" placeholder="Product Price">
                        </div>
                        
                        
                        <button type="submit" class="btn btn-default waves-effect waves-light">Save</button>
                        <button type="button" class="btn btn-danger waves-effect waves-light m-l-10">Cancel</button>
                    </form>
			    </div>
			</div>
            <div id="custom" class="modal-demo">
			    <button type="button" class="close" onclick="Custombox.close();">
			        <span>&times;</span><span class="sr-only">Close</span>
			    </button>
			    <h4 class="custom-modal-title">Add Custom Product</h4>
			    <div class="custom-modal-text text-left">
			        <form role="form">
                    <div class="form-group">
                            <label for="name">Product Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Product Name">
                        </div>
                        
                       
                        
                        <div class="form-group">
                            <label for="position">Product Price</label>
                            <input type="text" class="form-control" id="position" placeholder="Product Price">
                        </div>
                        
                        
                        <button type="submit" class="btn btn-default waves-effect waves-light">Save</button>
                        <button type="button" class="btn btn-danger waves-effect waves-light m-l-10">Cancel</button>
                    </form>
			    </div>
			</div>
@endsection