@extends('admin.layout.master')
@section('head-title', 'Seller Profile')
@section('content')
<!-- Start content -->
<div class="content">

<div class="wraper container">

    <!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
       

        <h4 class="page-title">Profile</h4>
        <ol class="breadcrumb">
            
            <li class="active">Edit Profile</li>
        </ol>
    </div>
</div>

    <div class="row">
        <div class="col-md-8 col-lg-8">
            <div class="profile-detail card-box">
                <div>
                    

                    

                    

                   
                    
                    <form class="form-horizontal" role="form">                                    
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Full Name</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" value="Full Name...">
	                                                </div>
	                                            </div>
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label" for="example-email">Company </label>
	                                                <div class="col-md-10">
	                                                    <input type="email" id="example-email" name="example-email" class="form-control" placeholder="Company ">
	                                                </div>
	                                            </div>
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Email</label>
	                                                <div class="col-md-10">
	                                                    <input type="password" class="form-control" value="Email">
	                                                </div>
	                                            </div>
	                                                                     
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Phone no</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" placeholder="Phone no">
	                                                </div>
	                                            </div>  
                                               <div class="form-group">
	                                                <label class="col-md-2 control-label">GST no</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" placeholder="GST no">
	                                                </div>
	                                            </div> 
                                               <div class="form-group">
	                                                <label class="col-md-2 control-label">Pan Card</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" placeholder="Pan Card">
	                                                </div>
	                                            </div> 
                                               <div class="form-group">
	                                                <label class="col-md-2 control-label">Addres1</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" placeholder="Addres">
	                                                </div>
	                                            </div>  
                                               <div class="form-group">
	                                                <label class="col-md-2 control-label">Addres2</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" placeholder="Addres2">
	                                                </div>
	                                            </div> 
                                               <div class="form-group">
	                                                <label class="col-sm-2 control-label">State</label>
	                                                <div class="col-sm-4">
	                                                    <select class="form-control">
	                                                        <option>1</option>
	                                                        <option>2</option>
	                                                        <option>3</option>
	                                                        <option>4</option>
	                                                        <option>5</option>
	                                                    </select>
	                                                    
	                                                </div>
                                                   <label class="col-sm-2 control-label">City</label>
                                                   <div class="col-sm-4">
	                                                    <select class="form-control">
	                                                        <option>1</option>
	                                                        <option>2</option>
	                                                        <option>3</option>
	                                                        <option>4</option>
	                                                        <option>5</option>
	                                                    </select>
	                                                    
	                                                </div>
	                                            </div> 
                                               <button type="submit" class="btn btn-default waves-effect waves-light btn-md">Submit</button>                                                                           
	                                            
	                                            
	                                            
	                           
	                                        </form>
                    
                   

                    

                    
                </div>

            </div>

            
        </div>


        

    </div>



</div> <!-- container -->
           
</div> <!-- content -->
@endsection