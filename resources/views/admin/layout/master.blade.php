<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="assets/images/favicon_1.ico">
	  <title>@yield('head-title')</title>
	<!-- Global stylesheets -->
	@include('admin.Layout.styles')
	<!-- /global stylesheets -->

	

</head>
<body class="widescreen fixed-left-void">

<!-- Begin page -->
<div id="wrapper">
@include('admin.include.topbar')
@include('admin.include.sidebar')

<div class="content-page">
@yield('content')

<footer class="footer text-right">
                    © Nita Ecommerce Private Limited 2019. All rights reserved.
 </footer>
</div>
@include('admin.include.right_sidebar')
</div>
@include('admin.Layout.scripts')	
</body>
</html>
